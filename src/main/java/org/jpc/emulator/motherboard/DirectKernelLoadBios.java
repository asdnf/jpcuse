package org.jpc.emulator.motherboard;

import java.io.IOException;

public class DirectKernelLoadBios extends Bios implements IODevice {

    public DirectKernelLoadBios(byte[] image) {
        super(image);
    }

    public DirectKernelLoadBios(String image) throws IOException {
        super(image);
    }

    public void ioPortWrite8(int address, int data) {

    }

    public void ioPortWrite16(int address, int data) {

    }

    public void ioPortWrite32(int address, int data) {

    }

    public int ioPortRead8(int address) {
        return 0;
    }

    public int ioPortRead16(int address) {
        return 0;
    }

    public int ioPortRead32(int address) {
        return 0;
    }

    public int[] ioPortsRequested() {
        return new int[0];
    }

    protected int loadAddress() {
        //return 0x100000 - length();
        return 0x100000;
        //return 0xc0000;
    }
}
