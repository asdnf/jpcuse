package org.jpc.emulator.peripheral;

public class FloatRef {
    public float value;

    public FloatRef() {
    }

    public FloatRef(float v) {
        value = v;
    }
}
